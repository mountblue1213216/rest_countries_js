import React, { useRef } from "react";
import { AiOutlineSearch } from "react-icons/ai";
import regions from "../regions";

const SearchAndFilter = ({ changeCategory, searchCountry }) => {
  return (
    <div className="filter mb-5">
      <div>
        <AiOutlineSearch />
        <input
          type="text"
          placeholder="Search for a  country"
          onChange={(event) => searchCountry(event.target.value)}
        />
      </div>

      <div>
        <select
          className="form-select"
          onChange={(event) => changeCategory(event.target.value)}
        >
          <option value="">Filter By Region</option>
          {regions.map((region) => (
            <option key={region} value={region}>
              {region}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};

export default SearchAndFilter;
