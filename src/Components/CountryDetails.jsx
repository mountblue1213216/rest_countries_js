import { useEffect, useState } from "react";
import axios from "axios";
import { useParams, Link } from "react-router-dom";
import { Route, Routes } from "react-router-dom";

const CountryDetails = () => {
  const { id } = useParams();
  const [countryDetails, setCountryDetails] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    setLoading(true);
    axios
      .get(`https://restcountries.com/v3.1/alpha/${id}`)
      .then((res) => {
        setCountryDetails(res.data);
        setLoading(false);
      })
      .catch((err) => {
        setError(err.message);
        setLoading(false);
      });
  }, [id]);

  return (
    <>
      {error && <p className="text-danger">{error}</p>}
      {isLoading && <div className="spinner-border"></div>}
      <div className="back-btn-container">
        <Link to={"/"}>
          <button type="button" className="back-btn">
            {" "}
            Back{" "}
          </button>
        </Link>
      </div>
      {countryDetails.map((country, index) => (
        <div key={index} className="country-full-detail-container">
          <div className="image-container">
            <img src={country.flags.svg} className="full-details-img" />
          </div>
          <div className="full-details-container">
            <h1>{country.name.common}</h1>
            <div className="sub-details">
              <div className="details-col">
                <div className="detail">
                  <p>Native Name:</p>
                  <p>{country.name.official && country.name.official}</p>
                </div>
                <div className="detail">
                  <p>Population:</p>
                  <p>{country.population}</p>
                </div>
                <div className="detail">
                  <p>Region:</p>
                  <p>{country.region}</p>
                </div>
                <div className="detail">
                  <p>Sub Region:</p>
                  <p>{country.subregion}</p>
                </div>
                <div className="detail">
                  <p>Capital:</p>
                  <p>{country.capital && country.capital[0]}</p>
                </div>
              </div>
              <div className="details-col">
                <div className="detail">
                  <p>Top LevelDomain:</p>
                  <p>{country.tld[0]}</p>
                </div>
                <div className="detail">
                  <p>Currencies:</p>
                  <p>{country.currencies && Object.keys(country.currencies)}</p>
                </div>
                <div className="detail">
                  <p>Languages:</p>
                  <p>
                    {country.languages &&
                      Object.values(country.languages).map((language) => (
                        <p key={language} className="language">
                          {language}
                        </p>
                      ))}
                  </p>
                </div>
              </div>
            </div>
            <div className="detail">
              <p>Border Countries: </p>
              {country.borders &&
                country.borders.map((border) => (
                  <Link to={`/alpha/${border}`} key={border} className="border">
                    <button
                      onClick={() => {
                        <Route
                          path={`/alpha/${id}`}
                          element={<CountryDetails />}
                        />;
                      }}
                    >
                      {border}
                    </button>
                  </Link>
                ))}
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export default CountryDetails;
