import React from "react";
import { Link } from "react-router-dom";

const Countries = ({ countriesData }) => {
  return (
    <div>
      <div className="flex_row mb-5">
        {countriesData.map((country, index) => (
          <>
            {" "}
            <Link
              to={`/alpha/${country.cca3}`}
              key={index}
              className="flex_column mb-3"
            >
              <img
                src={country.flags.png}
                alt={country.flag}
                className="mb-5"
              />
              <p>{country.name.common}</p>{" "}
              <p>Population: {country.population}</p>{" "}
              <p>Region: {country.region}</p> <p>Capital: {country.capital}</p>{" "}
            </Link>
          </>
        ))}
      </div>
    </div>
  );
};

export default Countries;
