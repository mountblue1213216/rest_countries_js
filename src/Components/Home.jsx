import { useEffect, useState } from "react";
import axios from "axios";
import SearchAndFilter from "./SearchAndFilter";
import Countries from "./Countries";
import { Link } from "react-router-dom";

const Home = () => {
  const [countries, setCountries] = useState([]);
  const [category, setCategory] = useState();
  const [searchCountry, setSearchcountry] = useState();
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [warning, setWarining] = useState("");

  useEffect(() => {
    setLoading(true);
    axios
      .get("https://restcountries.com/v3.1/all")
      .then((res) => {
        setCountries(res.data);
        setLoading(false);
      })
      .catch((err) => {
        setError(err.message);
        setLoading(false);
      });
  }, []);

  const handleCategory = (categories) => {
    setCategory(categories);
  };

  const filterCountry = (country) => {
    setSearchcountry(country.toLowerCase());
    setWarining(() => "Country not available");
  };

  let filteredCountires = countries.filter((data) => data.region === category);

  let country =
    filteredCountires.length === 0
      ? countries.filter((data) =>
          data.name.common.toLowerCase().includes(searchCountry)
        )
      : filteredCountires.filter((data) =>
          data.name.common.toLowerCase().includes(searchCountry)
        );

  return (
    <>
      {country.length === 0 && <p className="text-danger">{warning}</p>}
      {error && <p className="text-danger">{error}</p>}
      {isLoading && <div className="spinner-border"></div>}
      <div className="main">
        <SearchAndFilter
          changeCategory={handleCategory}
          searchCountry={filterCountry}
        />
        <Countries
          countriesData={
            country.length !== 0
              ? country
              : filteredCountires.length === 0
              ? countries
              : filteredCountires
          }
        />
      </div>
    </>
  );
};

export default Home;
