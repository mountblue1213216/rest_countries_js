import { BsMoon, BsMoonFill } from "react-icons/bs";

const Navbar = () => {
  return (
    <div className="navbar">
      <h4>Where in the world?</h4>
      <button type="button" className="btn btn-light">
        {" "}
        <BsMoon /> Dark
      </button>
    </div>
  );
};

export default Navbar;
