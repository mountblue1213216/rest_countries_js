import { Routes, Route } from "react-router-dom";
import Navbar from "./Components/Navbar";
import Home from "./Components/Home";
import CountryDetails from "./Components/CountryDetails";

function App() {
  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/alpha/:id" element={<CountryDetails />} />
      </Routes>
    </>
  );
}

export default App;
